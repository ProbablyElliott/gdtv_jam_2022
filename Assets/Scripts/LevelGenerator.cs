using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] LevelTileSet tileSet;

    string[] levelFiles;
    Level[] levels;


    private void Start()
    {
        levelFiles = System.IO.Directory.GetFiles("Assets\\Levels\\", "Room*.txt");
        Debug.Log("room files");
        levels = new Level[levelFiles.Length];
        int i = 0;
        foreach (string s in levelFiles)
        {
            Debug.Log(s);
            levels[i] = new Level(s);
            i++;
        }

        GenerateMap();

        GetComponent<CompositeCollider2D>().GenerateGeometry();
    }

    public void GenerateMap()
    {
        int rooms = tileSet.RoomsToGenerate();
        Vector2 exitDoor = Vector2.zero;
        List<TileType[,]> levelList = new List<TileType[,]>(); 

        for (int room = 0; room < rooms; room++)
        {
            Level currentLevel = GetRandomLevel();            
            TileType[,] levelTiles = currentLevel.GetLevelTiles();
            Vector2 roomOffset = new Vector2();

            if (room == 0 || room == rooms - 1)
            {
                // remove all but one doorway
                levelTiles = RemoveDoorways(levelTiles, DoorsInLevel(levelTiles)-1);
            }
            else if (room == 0 && room == rooms - 1)
            {
                // Remove All Doorways
                levelTiles = RemoveDoorways(levelTiles, DoorsInLevel(levelTiles));
            }

            GenerateRoom(levelTiles, room, roomOffset);
            levelList.Add(levelTiles);

            // If there is more rooms that need to be generated, find the position for the next door
            if (room + 1 < rooms)
            {
                List<Vector2> roomDoors = DoorsLocation(levelTiles);
                if (roomDoors.Count == 1)
                {
                    exitDoor = roomDoors[0];
                }
                else { 
                    
                }
            }
        }
    }

    private int DoorsInLevel(TileType[,] levelTiles)
    {
        int doors = 0;

        for(int i = 0; i < levelTiles.GetLength(0); i++)
        {
            for (int j = 0; j < levelTiles.GetLength(1); j++)
            {

                if (levelTiles[i, j] == TileType.DOOR)
                {
                    doors++;
                }
            }
        }

        return doors;
    }

    private List<Vector2> DoorsLocation(TileType[,] levelTiles)
    { 
        List<Vector2> rtnList = new List<Vector2>();

        for (int i = 0; i < levelTiles.GetLength(0); i++)
        {
            for (int j = 0; j < levelTiles.GetLength(1); j++)
            {
                if (levelTiles[i, j] == TileType.DOOR)
                {
                    rtnList.Add(new Vector2(i, j));
                }
            }
        }

        return rtnList;
    }

    private TileType[,] RemoveDoorways(TileType[,] tiles, int doorwaysToRemove, TileType swapValue=TileType.WALL)
    {
        TileType[,] rtnTiles = Utilities.Clone<TileType[,]>(tiles);
        int removedDoors = 0;

        for (int i = 0; i < rtnTiles.GetLength(0); i++)
        {
            for (int j = 0; j < rtnTiles.GetLength(1); j++)
            {
                if (removedDoors == doorwaysToRemove)
                    return rtnTiles;

                if (tiles[i, j] == TileType.DOOR)
                {
                    tiles[i, j] = swapValue;
                    removedDoors++;
                }
            }
        }

        return rtnTiles;
    }

    private Level GetRandomLevel()
    {
        return levels[Random.Range(0, levels.Length)];   
    }

    private void GenerateRoom(TileType[,] roomData, int roomNum, Vector3 offset)
    {
        GameObject room_holder = new GameObject($"Room_{roomNum}");
        room_holder.transform.parent = transform;
        int local_width = roomData.GetLength(0);
        int local_height = roomData.GetLength(1);

        for (int i = 0; i < roomData.GetLength(0); i++) {
            for (int j = 0; j < roomData.GetLength(1); j++) {
                string posStr = $"_{i}_{j}";
                string typeStr = roomData[i, j].ToString();
                string tileStr = $"Room_{roomNum}_{typeStr}_{posStr}";
                TileType tt = roomData[i, j];

                if (!IsPrefab(tt))
                {
                    GameObject go = new GameObject(tileStr);
                    go.transform.parent = room_holder.transform;
                    go.AddComponent(typeof(SpriteRenderer));
                    go.GetComponent<SpriteRenderer>().sprite = GetTileSprite(tt);
                    go.transform.position = offset + new Vector3(i, j, 0f);
                    if (tt == TileType.ENTRY)
                    {
                        go.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
                    }

                    if (CanSpawnJunk(tt) && tileSet.SpawnJunk())
                    {
                        go = new GameObject(tileStr.Replace(typeStr, typeStr + "_junk"));
                        go.transform.parent = room_holder.transform;
                        go.AddComponent(typeof(SpriteRenderer));
                        go.GetComponent<SpriteRenderer>().sprite = tileSet.GetJunkSprite();
                        go.GetComponent<SpriteRenderer>().rendererPriority = 1;
                        go.transform.position = offset + new Vector3(i, j, 0f);
                        go.transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 359f));
                    }
                }
                else
                {
                    GameObject prefab = GetTilePrefab(tt);
                    int rotVal = roomData[i, j] == TileType.CORNER ? GetCornerRotation(i, j, local_width, local_height) : GetWallRotation(i, j, local_width, local_height);
                    if (tt == TileType.OBSTACLE)
                    {
                        rotVal = Random.Range(0, 359);

                        GameObject go = new GameObject(tileStr);
                        go.transform.parent = room_holder.transform;
                        go.AddComponent(typeof(SpriteRenderer));
                        go.GetComponent<SpriteRenderer>().sprite = tileSet.GetFloorTile();
                        go.transform.position = offset + new Vector3(i, j, 0f);
                    }
                    else if (tt == TileType.EXIT)
                    {
                        rotVal = 0;
                    }
                    GameObject roomObject = Instantiate(prefab, offset + new Vector3(i, j, 0), Quaternion.Euler(0, 0, rotVal), room_holder.transform);
                    roomObject.name = tileStr;
                }
            }
        }
    }

    private bool IsPrefab(TileType tileType)
    {
        switch (tileType)
        {
            case TileType.WALL:
            case TileType.CORNER:
            case TileType.OBSTACLE:
            case TileType.EXIT:
                return true;
            default:
                return false;
        }
    }

    private bool CanSpawnJunk(TileType tileType)
    {
        if (tileType == TileType.TILE)
            return true;

        return false;
    }

    private Sprite GetTileSprite(TileType tileType)
    {
        switch (tileType)
        {
            case TileType.TILE:
                return tileSet.GetFloorTile();
            case TileType.DOOR:
                return tileSet.GetDoorSprite();
            case TileType.ENTRY:
                return tileSet.GetEntrySprite();
            default:
                return null;
        }
    }

    private GameObject GetTilePrefab(TileType tileType)
    {
        switch (tileType)
        {
            case TileType.WALL:
                return tileSet.GetWallTile();
            case TileType.CORNER:
                return tileSet.GetCornerTile();
            case TileType.OBSTACLE:
                return tileSet.GetObstacle();
            case TileType.EXIT:
                return tileSet.GetExitPrefab();
            default:
            case TileType.DOOR:
            case TileType.NOTHING:
            case TileType.TILE:
            case TileType.ENTRY:
                return null;
        }
    }

    private int GetWallRotation(int x, int y, int width, int height)
    {
        if (x == width - 1 && y > 0 && y < height - 1)
            return -90;
        if (x == 0 && y > 0 && y < height - 1)
            return 90;
        if (x > 0 && x < width - 1 && y == height - 1)
            return 0;
        return 180;
    }

    private int GetCornerRotation(int x, int y, int width, int height)    
    {
        if (x == 0 && y == height - 1)
            return 0;
        if (x == width - 1 && y == 0)
            return 180;
        if (x == width - 1 && y == height - 1)
            return 270;
        return 90;
    }
}
