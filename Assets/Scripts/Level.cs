using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public enum TileType
{
    // "-"
    NOTHING = 0,
    // "W" 
    WALL = 1,
	// "C"
    CORNER = 2,
    // "T"
    TILE = 3,
	// "D"
    DOOR = 4,
    // "O"
    OBSTACLE = 5,
    // "E"
    ENTRY = 6,
    // "X"
    EXIT = 7
}

public class Level
{
    private readonly TileType[,] tiles;

    public Level(string textFile) {
        List<string> levelStrings = new List<string>();
        StreamReader sr = new StreamReader(textFile);
        int width = 0;
        int height = 0;
        while (!sr.EndOfStream)
        {
            string add_s = sr.ReadLine();
            levelStrings.Add(add_s);
            if (add_s.Length > width)
                width = add_s.Length;
            height++;
        }

        tiles = new TileType[width, height];
        height = 0;
        foreach (string s in levelStrings)
        {
            width = 0;
            foreach (char c in s.ToCharArray())
            {
                tiles[width, height] = CharToTile(c);
                width++;
            }
            height++;
        }
    }

    private TileType CharToTile(char c)
    {
        switch (c) {
            case 'w':
            case 'W':
                return TileType.WALL;
            case 'c':
            case 'C':
                return TileType.CORNER;
            case 't':
            case 'T':
                return TileType.TILE;
            case 'd':
            case 'D':
                return TileType.DOOR;
            case 'o':
            case 'O':
                return TileType.OBSTACLE;
            case 'e':
            case 'E':
                return TileType.ENTRY;
            case 'x':
            case 'X':
                return TileType.EXIT;
            default:
                return TileType.NOTHING;
        }
    }

    public TileType[,] GetLevelTiles()
    {
        return tiles;
    }
}
