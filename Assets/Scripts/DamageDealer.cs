using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    [SerializeField] float damage = 1f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent(out Health h))
        {
            // if in the future I want to change it so enemies can kill eachother, change this to just compare the roots, not the tags
            if (collision.transform.root.tag == transform.root.tag)
            {
                Debug.Log("Stop hitting yourself");
                return;
            }
            Debug.Log($"Damage dealt by: {collision.gameObject}");
            h.Damage(damage, transform);
        }
    }
}
