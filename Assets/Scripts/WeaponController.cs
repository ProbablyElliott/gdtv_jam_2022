using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [SerializeField] GameObject weaponPrefab;
    Transform playerWeapon;

    // Start is called before the first frame update
    void Start()
    {
        playerWeapon = transform.Find("Hand").Find("Weapon");

        UpdateWeapon();
    }

    public void SetWeapon(GameObject weapon)
    {
        weaponPrefab = weapon;

        playerWeapon.DetachChildren();

        UpdateWeapon();
    }

    private void UpdateWeapon()
    {
        GameObject wep = Instantiate(weaponPrefab, playerWeapon.position + weaponPrefab.transform.position, weaponPrefab.transform.rotation);
        wep.transform.parent = playerWeapon;

        playerWeapon.GetComponent<CompositeCollider2D>().GenerateGeometry();
    }
}
