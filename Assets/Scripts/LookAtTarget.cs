using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTarget : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField][Min(0.1f)] float lookSpeed;


    // Update is called once per frame
    void FixedUpdate()
    {
        if (target != null)
        { 
            float step = lookSpeed * Time.deltaTime;
            float angle = Mathf.Rad2Deg * (Mathf.Atan2(transform.position.y - target.position.y, transform.position.x - target.position.x)) -90f;
        
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0f,0f, angle), step);
        }
    }
}
