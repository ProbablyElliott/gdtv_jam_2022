using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] float maxHealth = 50f;
    [SerializeField] float currentHealth = 50f;


    [SerializeField] float invunerableTime = 0.75f;
    bool invunerable = false;


    SpriteRenderer spriteRenderer;
    Rigidbody2D rb;

    private void Start()
    {
        currentHealth = maxHealth;
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
    }

    public void Damage(float damage, Transform damageDealer)
    {
        if (!invunerable)
        {
            invunerable = true;
            Debug.Log("Damage being dealth");
            currentHealth -= damage;
            StartCoroutine(DamageVisual());

            Knockback(damageDealer);

            if (currentHealth <= 0f)
            {
                Die();
            }
        }
    }


    IEnumerator DamageVisual()
    {
        Debug.Log("Starting damage visual");
        for (int i = 0; i < 6; i++) {
            Color highlight = i % 2 == 1 ? Color.white : Color.red;
            spriteRenderer.color = highlight;
            yield return new WaitForSeconds(invunerableTime / 6f);
        }

        spriteRenderer.color = Color.white;
        invunerable = false;
    }

    private void Knockback(Transform dealerTransform)
    {
        float pushPower = 1f;
        //target is the enemy or target :p
        //calculate a direction away from the transform (or player whatever it is doing the pushing)
        Vector2 targetHeadingAway = (transform.position - dealerTransform.position).normalized;

        Debug.Log($"value: {targetHeadingAway}");
        if (transform.TryGetComponent(out PlayerMovement pm))
        {
            pm.Knockback(invunerableTime);
        }
        rb.AddForce(targetHeadingAway * pushPower, ForceMode2D.Impulse);
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}
