using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "ScriptableObjects/LevelTileSet")]
public class LevelTileSet : ScriptableObject
{
    [SerializeField] List<Sprite> floorTiles;
    [SerializeField] List<Sprite> junkTiles;
    [SerializeField][Range(0f,1f)][Tooltip("Value to exceed in order to spawn junk, checked against a 0f-1f random number")] float junkThreshold;

    [SerializeField] List<GameObject> wallTiles;
    [SerializeField] List<GameObject> cornerWallTiles;
    [SerializeField] List<Sprite> doorwayTiles;
    [SerializeField] List<GameObject> obstableTiles;

    [SerializeField] Sprite entrySprite;
    [SerializeField] GameObject exitGameObject;

    [SerializeField][Min(1)] int minRooms;
    [SerializeField][Min(1)] int maxRooms;

    public Sprite GetFloorTile()
    {
        return floorTiles[Random.Range(0, floorTiles.Count - 1)];
    }

    public GameObject GetWallTile()
    {
        return wallTiles[Random.Range(0, wallTiles.Count - 1)];
    }

    public GameObject GetCornerTile()
    {
        return cornerWallTiles[Random.Range(0, cornerWallTiles.Count - 1)];
    }

    public int RoomsToGenerate()
    {
        return Random.Range(minRooms, maxRooms);
    }

    public bool SpawnJunk()
    {
        return Random.Range(0f, 1f) > junkThreshold;
    }

    public Sprite GetJunkSprite()
    { 
        return junkTiles[Random.Range(0, junkTiles.Count - 1)];
    }

    public GameObject GetObstacle()
    { 
        return obstableTiles[Random.Range(0, obstableTiles.Count - 1)];
    }

    public GameObject GetExitPrefab()
    {
        return exitGameObject;
    }

    public Sprite GetDoorSprite()
    { 
        return doorwayTiles[Random.Range(0, doorwayTiles.Count - 1)];
    }

    public Sprite GetEntrySprite()
    {
        return entrySprite;
    }
}
