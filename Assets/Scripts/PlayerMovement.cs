using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float lookSpeed = 5f;

    Rigidbody2D body;
    Camera primaryCamera;

    Vector2 movement;
    Vector2 mousePos;

    bool knockedBack = false;

    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        primaryCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");

        mousePos = primaryCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    private void FixedUpdate()
    {
        if (!knockedBack)
        {
            body.MovePosition(body.position + movement * moveSpeed * Time.fixedDeltaTime);
            //body.AddForce(movement * speed * Time.fixedDeltaTime);

            Vector2 lookDir = mousePos - body.position;
            float angle = Mathf.Clamp(Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg + 90f, 0f, 360f);
            body.rotation = Mathf.LerpAngle(body.rotation, angle, lookSpeed * Time.deltaTime);
        }
    }

    public void Knockback(float duration)
    {
        knockedBack = true;
        StartCoroutine(Knockedback(duration));
    }

    IEnumerator Knockedback(float durationInSeconds)
    {
        yield return new WaitForSeconds(durationInSeconds);
        knockedBack = false;
    }
}
